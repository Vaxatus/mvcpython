class Singleton:
    __instance = None

    @staticmethod
    def get_instance():
        """ Static access method. """
        if Singleton.__instance is None:
            Singleton()
        return Singleton.__instance

    def __init__(self):
        """ Virtually private constructor. """
        if Singleton.__instance is not None:
            raise Exception("This class is a singleton!")
        else:
            self.name = "DDDDDDD"
            Singleton.__instance = self

    def set_name(self, name):
        self.name = name

    def get_name(self):
        return self.name

if __name__ == '__main__':

    s = Singleton()
    print(s)
    print(s.get_name())
    s.set_name("MMMM")

    # s = Singleton()
    # print(s)
    # print(s.get_name())


    s = Singleton.get_instance()
    print(s)
    print(s.get_name())

    s = Singleton.get_instance()
    print(s)
    print(s.get_name())
