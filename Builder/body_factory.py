class Body(object):
    shape = ""

    def get_shape(self):
        return self.shape


class Suv(Body):
    shape = "SUV"


class Cupe(Body):
    shape = "CUPE"


class Hatchback(Body):
    shape = "HATCHBACK"


class BodyFactory():
    def create_body(self, typ):
        targetclass = typ.capitalize()
        return globals()[targetclass]()


def get_body(type):
    body = BodyFactory()
    return body.create_body(type)



# def get_suv():
#     body = BodyFactory()
#     return body.create_body('suv')
#
#
# def get_hatchback():
#     body = BodyFactory()
#     return body.create_body('hatchback')
#
#
# def get_cupe():
#     body = BodyFactory()
#     return body.create_body('cupe')