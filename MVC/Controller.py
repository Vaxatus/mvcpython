from MVC.Model import Person
from MVC import View


def showAll():
   #gets list of all Person objects
   people_in_db = Person.getAll()
   #calls view
   return View.showAllView(people_in_db)


def start():
   View.startView()
   if input() == 'y':
      return showAll()
   else:
      return View.endView()

if __name__ == "__main__":
   #running controller function
   start()